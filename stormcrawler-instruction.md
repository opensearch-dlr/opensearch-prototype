# Storm Crawler

## I. [Setup a Storm Cluster and Start Storm UI]((https://storm.apache.org/releases/2.1.0/Setting-up-a-Storm-cluster.html))

### Requirements
- I used Windows 10 
- Make sure you have JDK 1.8+, Java 8+, Python 2.6.6+ 


**Note: It seems that higher Version (e.g. Java 11) of Java are not supported. You should preferably use Java 8.** 

- Install Apache Maven (current version: 3.6.3)
    - [Download](https://maven.apache.org/download.cgi) 
    - [Install](https://maven.apache.org/install.html)

- Install Apache Storm (current version: 2.2.0)
    - [Download](http://storm.apache.org/downloads.html)
    - [Tutorial](http://storm.apache.org/releases/current/Tutorial.html)
    - Note (for later): The version of Storm to use must match the one defined in the pom.xml file of your topology.
    - Add `{YOUR_PATH}\apache-storm-2.2.0\bin` to your `$PATH`.
    - Create `$STORM_HOME` var pointing to '`{YOUR_PATH}\apache-storm-2.2.0`

- Install Zookeeper (current version: 3.6.2)
    - [Download](https://www.apache.org/dyn/closer.lua/zookeeper/zookeeper-3.6.2/apache-zookeeper-3.6.2-bin.tar.gz)
    -  Extract the tar file (e.g., using
    `tar -xvzf TAR_FILE_PATH -C FOLDER_DESTINATION_PATH`)


**Note: Please make sure you are actually using Apache Storm and Storm Crawler v2.x . There are actually incompatible 1.x versions, which will lead to problems if combined with the wrong versions of the rest of the software.**

### Cluster Setup

**Note** If you already did the setup steps, then you just need to run the commands only: under *run* bullet point of each sub-section.

**PS:** On Windows, you can use `start \B *cmd*` to avoid running each command in a cmd window (for example: `start \B storm nimbus` instead of `storm nimbus`).


#### 1. Run Zookeeper
- Make sure `{YOUR_PATH}\apache-zookeeper-3.6.2\conf\zoo.cfg` is created:
    - There is a `zoo_sample.cfg`, create another `zoo.cfg` (for now: copy paste content of `zoo_sample.cfg`)
        - Change the `dataDir` to a valid path for an empty folder
        - The default `clientPort` is *2181* - Nimbus uses this port by default to connect to ZooKeeper.
- run `{YOUR_PATH}\apache-zookeeper-3.6.2\bin\zkServer.{cmd|sh}` in the console:

    ```
        {YOUR_PATH}\apache-zookeeper-3.6.2\bin\zkServer.{cmd|sh}
    ```

#### 2. Run Nimbus

- Got to `storm.yaml` and add/change:

    ```

        storm.zookeeper.servers:
            - "0.0.0.0"
        storm.local.dir: "$STORM_HOME\\storm-local" # linux "$STORM_HOM/storm-local"
        nimbus.host: "0.0.0.0"

        supervisor.slots.ports:
            - 6700
            - 6701
            - 6702
            - 6703

        ui.port: 8888
    ```

- run

    ```
        $storm nimbus
    ```


    Check the `{YOUR_PATH}\apache-storm-2.2.0\logs\nimbus.log` to make sure everything is okay.


#### 3. Run Supervisor

- run

    ```
        $storm supervisor
    ```

    Check the `{YOUR_PATH}\apache-storm-2.2.0\logs\supervisor.log` to make sure everything is okay.

#### 4. Run Storm UI
- run

    ```
        $storm ui
    ```

    Check the `{YOUR_PATH}\apache-storm-2.2.0\logs\ui.log` to make sure everything is okay.
    Access the Storm UI via `http://localhost:8888`

_________________

## II. Elastic Search and Kibana

#### ElasticSearch
- [Install & run ElasticSearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/zip-windows.html)
- set `ES_HOME` to `{YOUR_PATH}\elasticsearch-7.12.0\`
- run elastic search from command line:
    - Elasticsearch loads its configuration from the %ES_HOME%\config\elasticsearch.yml file by default. The format of this config file is explained in [Configuring Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/settings.html). 

    - Any settings that can be specified in the config file can also be specified on the command line, using the -E syntax as follows:

    ```
         %ES_HOME%\bin\elasticsearch.bat -Ecluster.name=opensearch_cluster -Enode.name=es_node
    ```

    - Go to `http://localhost:9200` and you should see something like this:

        ```json
            {
                "name" : "es_node",
                "cluster_name" : "opensearch_cluster",
                "cluster_uuid" : "9Q7BbU36SfiB5s0qd138iw",
                "version" : {
                    "number" : "7.12.0",
                    "build_flavor" : "default",
                    "build_type" : "zip",
                    "build_hash" : "78722783c38caa25a70982b5b042074cde5d3b3a",
                    "build_date" : "2021-03-18T06:17:15.410153305Z",
                    "build_snapshot" : false,
                    "lucene_version" : "8.8.0",
                    "minimum_wire_compatibility_version" : "6.8.0",
                    "minimum_index_compatibility_version" : "6.0.0-beta1"
                },
                "tagline" : "You Know, for Search"
            }
        ```
  
#### ElasticSearch
- [Install and run Kibana](https://www.elastic.co/downloads/kibana): download, unzip and follow the installation steps
- Open config/kibana.yml in an editor and set `elasticsearch.hosts` to `[http://localhost:9200]`
- run:
    ```
         {your_path}\bin\kibana.bat 
    ```

- Go to `http://localhost:5601`

________

## III. Create Storm crawler +  Elastic search using MVN

The project is already created in this repository under *prototype/crawler* with the following characteristics:

- artificatId: `de.dlr.sc.crawler`
- groupId: `crawler`
- package name: `de.dlr.sc.crawler`

___________________________

No need to do the steps below, I put them here for reference:


- The guide is found [here](https://github.com/DigitalPebble/storm-crawler/tree/master/external/elasticsearch).
- Maven is used to create the project along with all dependencies for stormcrawler and elastic search:

    ```
        mvn archetype:generate -DarchetypeGroupId=com.digitalpebble.stormcrawler -DarchetypeArtifactId=storm-crawler-elasticsearch-archetype -DarchetypeVersion=1.17
    ```

    
- You will be prompted to enter an artificatId: `de.dlr.sc.crawler`, groupId: `crawler` and package name: `de.dlr.sc.crawler` . 

- Then the project will be created succesfully with all the required dependencies and the following folders/files under *crawler* folder:

    ![mvn project folders and files](diagrams/mvn_stormcrawler_files.PNG)



**Note: Using the 1.17 archetype may lead to incompatibility. So if it does not work, try:**

    ```
       mvn archetype:generate -DarchetypeGroupId=com.digitalpebble.stormcrawler -DarchetypeArtifactId=storm-crawler-elasticsearch-archetype -DarchetypeVersion=2.1
    ```


