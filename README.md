<div align="center">
  <img src="diagrams/logo.png" width="700" alt="Opensearch@DLR Logo">
</div>

---
# OpenSearch Project at SC-IVS

The aim of this project is to build a prototype for an information retrieval system for DLR domains. 
This includes elib as well as all publicly accessible pages of the DLR.

## Background
Data provide the raw materials for research, innovation and business in the 21st century. Open, unbiased and transparent access to information is therefore a basic prerequisite for the free development of a digital society. 
However, internet searches have become highly monopolised. This has an impact on access to information and knowledge, hinders scientific research and industrial activities, and is detrimental to Europe’s digital sovereignty.

The **OpenSearch@DLR project** is addressing this area of digital technology, which is strategically important for the German Aerospace Center (Deutsches Zentrum für Luft- und Raumfahrt; DLR) and for scientific research. 
It explores concepts for a distributed and open infrastructure for next-generation internet search. The aim is to ensure that the wealth of data on the internet is harnessed more effectively through impartial searching 
and that data and information are disseminated to provide a basis for free research and innovation. [Read more](https://www.dlr.de/content/en/articles/digitalisation/digitalisation-project-opensearchatdlr.html)

A search engine building blocks are as follow [\[1\]](http://ciir.cs.umass.edu/downloads/SEIRiP.pdf) [\[2\]](https://webis.de/downloads/lecturenotes/information-retrieval/unit-en-ir-architecture.pdf):


<img src="diagrams/search_engine_building_blocks.png" width="700">

## Crawler - [Text Acquisition]
In order to be able to index web pages, a crawler is required that retrieves these web pages and stores them. Within the **OpenSearch@DLR** project, [Storm Crawler](https://github.com/DigitalPebble/storm-crawler) is used. 
It is based on [Apache Storm](https://storm.apache.org/) and allows to index web pages with a distributed system. An important aspect is also that both projects are open source.

For a first impression it is worth to have a look at this [presentation](material/crawler.pptx). The crawler [architecture](crawler_architecture.md) page offers a big picture. For a detailed insight the 
[wiki](https://gitlab.com/opensearch-dlr/opensearch-prototype/-/wikis/home) offers different topic pages.

## Preprocessor - [Text Transformation]
The processing of the data runs through a task scheduler called Prefect. To this scheduler all possible workflows can be sent in the form of python files. This scheduler executes these workflows at a specified interval 
and thus ensures that the data is always up-to-date.
