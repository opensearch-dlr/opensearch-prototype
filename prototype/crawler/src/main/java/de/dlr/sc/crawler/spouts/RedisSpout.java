package de.dlr.sc.crawler.spouts;

import com.digitalpebble.stormcrawler.Constants;
import com.digitalpebble.stormcrawler.Metadata;
import com.digitalpebble.stormcrawler.persistence.Status;
import com.digitalpebble.stormcrawler.util.ConfUtils;
import de.dlr.sc.crawler.Util.CrawlerEntry;
import de.dlr.sc.crawler.Util.DomainEntry;
import de.dlr.sc.crawler.Util.PathEntry;
import de.dlr.sc.crawler.Util.ReadConfigFromFile;
import de.dlr.sc.crawler.filters.DomainFilter;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.*;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Simple class that wraps the incoming data from Redis in a comfortable way.
 * !! Important !! Works only on key based subscriptions like "__key*__:de/dlr/*"
 * Subscriptions on Events like "__key*__:SET" are NOT SUPPORTED!
 */
class MessageContainer {
    // Channel like: __keyspace@0__:de/dlr/subdomain
    // Message: KEY OP like SET
    public final String message;
    public final String channel;
    public final String pattern;
    public String url;

    public MessageContainer(String message, String channel, String pattern) {
        this.message = message;
        this.channel = channel;
        this.pattern = pattern;
        this.url = null;
    }

    /**
     * Domains in Redis are reverse Domain encoded. This method transforms reverse Domains to FQDNs.
     *
     * @return FQDN
     */
    public String buildDomainFromKV() {
        // This method is more verbose for readability.
        // ["__keyspace@0__", "de/dlr/subdomain"]
        String[] split1 = channel.split(":", 0);
        // ["de", "dlr", "subdomain"]
        String[] split2 = split1[1].split("/", 0);
        // Transform reverse Domain. ["de", "dlr", "subdomain"] -> ["subdomain", "dlr", "de"]
        ArrayList<String> nameList = new ArrayList<>(Arrays.asList(split2));
        Collections.reverse(nameList);
        // return transformation from ["subdomain", "dlr", "de"] to "subdomain.dlr.de"
        return String.join(".", nameList);
    }

    /**
     * channel "__keyspace@0__:de/dlr/subdomain" will return "de/dlr/subdomain"
     *
     * @return Key in redis
     */
    public String getKey() {
        return channel.split(":")[1];
    }


    @Override
    public String toString() {
        return "MessageContainer{" +
                "message='" + message + '\'' +
                ", channel='" + channel + '\'' +
                ", pattern='" + pattern + '\'' +
                '}';
    }
}

/**
 * Listener which reacts to change in redis
 */
class Listener extends JedisPubSub {
    private final BlockingQueue<MessageContainer> queue;

    public Listener(BlockingQueue<MessageContainer> queue) {
        this.queue = queue;
    }

    public void onMessage(String channel, String message) {
        System.out.println("onMessage Received message:" + message + " On channel " + channel);
    }

    public void onSubscribe(String channel, int subscribedChannels) {
        System.out.println("onSubscribe Received subscribedChannels:" + subscribedChannels + " On channel " + channel);

    }

    public void onUnsubscribe(String channel, int subscribedChannels) {
        System.out.println("onUnsubscribe Received subscribedChannels:" + subscribedChannels + " On channel " + channel);

    }

    public void onPSubscribe(String pattern, int subscribedChannels) {
        System.out.println("onPSubscribe Received subscribedChannels:" + subscribedChannels + " On pattern " + pattern);

    }

    public void onPUnsubscribe(String pattern, int subscribedChannels) {
        System.out.println("onPUnsubscribe Received subscribedChannels:" + subscribedChannels + " On pattern " + pattern);

    }

    public void onPMessage(String pattern, String channel, String message) {
        System.out.println("onPMessage Received message: " + message + " On pattern " + channel);
        if (!channel.contains("__keyevent")) { // Keyevents like set etc. are not supported at the moment...
            queue.add(new MessageContainer(message, channel, pattern));
        }
    }
}

/**
 * Listener that reacts to new/edited keys in redis and puts them into the queue redisEventQueue.
 */
class RedisWatcher implements Runnable {

    private final BlockingQueue<MessageContainer> queue;
    private final JedisPool pool;
    private final String subscribePattern;


    public RedisWatcher(JedisPool pool, BlockingQueue<MessageContainer> redisEventQueue, String subscribePattern) {
        this.queue = redisEventQueue;
        this.pool = pool;
        this.subscribePattern = subscribePattern;
    }

    public void run() {
        Listener listener = new Listener(this.queue);
        try (Jedis jedis = pool.getResource()) {
            jedis.configSet("notify-keyspace-events", "KEA");
            // Channel like: __keyspace@0__:de/dlr/subdomain
            // Message: KEY OP like SET
            // This is a blocking OP
            jedis.psubscribe(listener, "__key*__:" + subscribePattern);
        }
    }
}

/**
 * Get Keys from redis which have been already added, before this crawler instance was started
 */
class RedisCollector implements Runnable {

    private final BlockingQueue<MessageContainer> queue;
    private final JedisPool pool;
    private final String subscribePattern;


    public RedisCollector(JedisPool pool, BlockingQueue<MessageContainer> redisEventQueue, String subscribePattern) {
        this.queue = redisEventQueue;
        this.pool = pool;
        this.subscribePattern = subscribePattern;
    }

    public void run() {
        try (Jedis jedis = pool.getResource()) {
            String cursor = "0";
            ScanParams sp = new ScanParams();
            sp.match(subscribePattern);
            do {
                ScanResult<String> scanResult = jedis.scan(cursor, sp);
                for (String key : scanResult.getResult()) {
                    System.out.println("[RedisCollector] Add " + key + " to Queue...");
                    queue.add(new MessageContainer("Collector fetch", "__redis_collector__:" + key, subscribePattern));
                }
                cursor = scanResult.getCursor();
            } while (!"0".equals(cursor));
        }
    }
}

/**
 * Consumer that blocks on redisEventQueue. If there are new keys in the queue,
 * it fetches them from the queue and retrieves the contents. The content of the key gives information
 * about which protocol and if there is a path.
 * From this information the consumer builds a URL and puts it into the queue newDomainQueue.
 */
class RedisConsumeAndTransform implements Runnable {

    private final BlockingQueue<MessageContainer> redisEventQueue;
    private final ConcurrentLinkedQueue<MessageContainer> newDomainQueue;
    private final JedisPool pool;

    public static final Logger LOG = LoggerFactory.getLogger(RedisConsumeAndTransform.class);


    public RedisConsumeAndTransform(JedisPool pool, BlockingQueue<MessageContainer> redisEventQueue, ConcurrentLinkedQueue<MessageContainer> newDomainQueue) {
        this.redisEventQueue = redisEventQueue;
        this.newDomainQueue = newDomainQueue;
        this.pool = pool;
    }

    @Override
    @SuppressWarnings("InfiniteLoopStatement")
    public void run() {
        while (true) {
            try {
                // Wait for new key like "de/dlr/elib"
                // Blocking OP
                MessageContainer msg = redisEventQueue.take();
                try (Jedis jedis = pool.getResource()) {
                    DomainEntry domainEntry = DomainEntry.createFromJson(jedis.get(msg.getKey()));
                    for (PathEntry path : domainEntry.paths) {
                        // Build URL
                        msg.url = path.protocol + "://" + msg.buildDomainFromKV() + path.path;
                        LOG.info("+++ new url {}]", msg.url);
                        newDomainQueue.add(msg);
                    }

                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

/**
 * This spout monitors Redis using Redis Keyspace Notifications (https://redis.io/topics/notifications).
 * In one thread, Redis is monitored. If there are changes, a listener is notified.
 * The listener puts the information into a queue. Another thread reads this queue
 * and transforms the data so that the result is a URL. For this another queue is used.
 * This queue uses nextTuple() to deliver new URLs.
 * Blueprint:
 * (Redis) <--watch-- (redisWatcher) --put--> (redisEventQueue) <--get-- (redisConsumeAndTransform) --put--> newDomainQueue <--get nextTuple()
 */
public class RedisSpout extends BaseRichSpout {

    public static final Logger LOG = LoggerFactory.getLogger(RedisSpout.class);

    // Blocking queue to send MSG Containers from startRedisWatcher to redisConsumeAndTransform
    private final BlockingQueue<MessageContainer> redisEventQueue = new LinkedBlockingDeque<>();
    // NON blocking queue which is used in nextTuple to get Domains from Redis
    private final ConcurrentLinkedQueue<MessageContainer> newURLQueue = new ConcurrentLinkedQueue<>();
    // TODO: Read values from config file
    private final String redisHost;

    private String crawlerName;
    private JedisPool pool;

    private SpoutOutputCollector _collector;

    public RedisSpout(String redisServerAddress) {
        redisHost = redisServerAddress;
    }

    /**
     * Create Jedis Instance and start the Threads
     */
    @Override
    public void open(Map conf, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
        String data = ReadConfigFromFile.getString(DomainFilter.class, "subscribe-pattern.txt");
        String subscribePattern = Objects.requireNonNull(data);
        crawlerName = ConfUtils.getString(conf, "crawlerName");

        pool = new JedisPool(new JedisPoolConfig(), redisHost);
        _collector = spoutOutputCollector;

        // Register this crawler in redis
        try (Jedis jedis = pool.getResource()) {
            String domainRegex = ReadConfigFromFile.getString(DomainFilter.class, "domain-regex.txt");
            CrawlerEntry crawler = new CrawlerEntry(
                    ConfUtils.getString(conf, "crawlerName"),
                    domainRegex,
                    subscribePattern
            );
            // Crawler entries start with "+" by convention
            jedis.set("+" + crawler.name, crawler.toJson());
        }

        // Consume elements which were put into the local queue
        Thread redisConsumeAndTransform = new Thread(new RedisConsumeAndTransform(pool, redisEventQueue, newURLQueue));
        redisConsumeAndTransform.start();

        // Watch for messages the redis instance is sending
        Thread redisWatcher = new Thread(new RedisWatcher(pool, redisEventQueue, subscribePattern));
        redisWatcher.start();

        // Collect Keys already present
        Thread redisCollector = new Thread(new RedisCollector(pool, redisEventQueue, subscribePattern));
        redisCollector.start();


    }

    @Override
    public void nextTuple() {
        if (!newURLQueue.isEmpty()) {
            MessageContainer msg = newURLQueue.poll();
            List<Object> fields = new Values(msg.url, new Metadata(), Status.DISCOVERED);
            this._collector.emit(Constants.StatusStreamName, fields, fields.get(0));
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declareStream(Constants.StatusStreamName, new Fields("url", "metadata", "status"));
    }

    /**
     * Delete crawler entity in redis, so others know that this crawler is no longer active.
     * <p>
     * Called when this spout is going to be shutdown. There is no guarantee that close will be called,
     * because the supervisor kill -9's worker processes on the cluster.
     */
    @Override
    public void close() {
        try (Jedis jedis = pool.getResource()) {
            jedis.del("+" + crawlerName);
        }
        super.close();
    }
}
