package de.dlr.sc.crawler.Util;


import com.google.gson.Gson;

import java.util.LinkedList;
import java.util.List;

public class DomainEntry {
    public List<PathEntry> paths = new LinkedList<>();
    public String foundBy;

    public static DomainEntry createFromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, DomainEntry.class);
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public DomainEntry() {
        // no-args constructor
    }
}
