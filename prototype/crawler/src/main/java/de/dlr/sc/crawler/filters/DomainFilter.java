package de.dlr.sc.crawler.filters;

import com.digitalpebble.stormcrawler.Metadata;
import com.digitalpebble.stormcrawler.filtering.URLFilter;
import com.digitalpebble.stormcrawler.util.ConfUtils;
import com.fasterxml.jackson.databind.JsonNode;
import de.dlr.sc.crawler.Util.DomainEntry;
import de.dlr.sc.crawler.Util.PathEntry;
import de.dlr.sc.crawler.Util.ReadConfigFromFile;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Pattern;


/**
 * Filters urls based on the top level domains defined in DomainFilter param.
 * If this instance is not responsible, push URL to redis
 */
public class DomainFilter implements URLFilter {

    private final BlockingQueue<URL> urlsToDispatch = new LinkedBlockingQueue<>();
    private Pattern pattern;
    private String crawlerName;

    /**
     * @param sourceUrl Not used
     * @param sourceMetadata Not used
     * @param urlToFilter URL which comes from status stream
     * @return URL string if URL matches Filter. If URL is not within whitelist, it gets dispatched to redis
     */
    @Override
    public String filter(URL sourceUrl, Metadata sourceMetadata, String urlToFilter) {
        try {
            URL url2check = new URL(urlToFilter);
            // Ignore links like mailto links
            if (!url2check.getProtocol().equals("http") && !url2check.getProtocol().equals("https")) {
                LOG.info("Unsupported protocol: "+ url2check);
                return null;
            }
            if (pattern.matcher(urlToFilter).find()) {
                LOG.info(urlToFilter + " matches whitelist regex");
                return urlToFilter;
            } else {
                LOG.info("Do not index " + urlToFilter + ", since domain " + url2check.getHost() + " is not whitelisted...");
                // Send URL to dispatcher
                urlsToDispatch.add(url2check);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Called when this filter is being initialized
     * Params:
     * @param stormConf – The Storm configuration used for the ParserBolt
     * @param filterParams – the filter specific configuration. Never null
     */
    @Override
    @SuppressWarnings("unchecked")

    public void configure(Map stormConf, JsonNode filterParams) {
        String redisServerAddress = ConfUtils.getString(stormConf,
                "redisServerAddress");
        String crawlerName = ConfUtils.getString(stormConf, "crawlerName");
        String data = ReadConfigFromFile.getString(DomainFilter.class, "domain-regex.txt");
        pattern = Pattern.compile(Objects.requireNonNull(data));
        Thread dispatchUrls = new Thread(new DispatchUrls(urlsToDispatch, redisServerAddress, crawlerName));
        dispatchUrls.start();
        LOG.info("Adding Domain Regex: " + pattern);
    }
}

class DispatchUrls implements Runnable {

    private final BlockingQueue<URL> urlsToDispatch;
    private final JedisPool pool;
    private final String crawlerName;


    public DispatchUrls(BlockingQueue<URL> urlsToDispatch, String redisServerAddress, String crawlerName) {
        this.urlsToDispatch = urlsToDispatch;
        this.pool = new JedisPool(new JedisPoolConfig(), redisServerAddress);
        this.crawlerName = crawlerName;
    }

    @Override
    @SuppressWarnings("InfiniteLoopStatement")
    public void run() {
        Jedis jedis = pool.getResource();
        while (true) {
            try {
                // Wait for new url to dispatch
                URL url = urlsToDispatch.take();
                System.out.println("****DispatchUrls " + url);
                // Build key
                // TODO check: authority vs host. Which is better?
                List<String> reverseDomain = Arrays.asList(url.getHost().split("\\."));
                Collections.reverse(reverseDomain);
                String key = String.join("/", reverseDomain);
                String alreadyAvail = jedis.get(key);
                // Domain is new
                if (alreadyAvail == null) {
                    DomainEntry newDomain = new DomainEntry();
                    newDomain.foundBy = crawlerName;
                    newDomain.paths.add(
                            new PathEntry()
                                    .setPath(url.getPath())
                                    .setProtocol(url.getProtocol())
                    );
                    jedis.set(key, newDomain.toJson());
                } else { // Domain is already specified.
                    DomainEntry knownDomain = DomainEntry.createFromJson(alreadyAvail);
                    PathEntry newPath = new PathEntry()
                            .setPath(url.getPath())
                            .setProtocol(url.getProtocol())
                            .setFoundBy(crawlerName);
                    // Is the path new?
                    if (!knownDomain.paths.contains(newPath)) {
                        knownDomain.paths.add(newPath);
                        jedis.set(key, knownDomain.toJson());
                    }
                }// Otherwise, the path is ignored because it is already in redis

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
