package de.dlr.sc.crawler.bolts;

import com.digitalpebble.stormcrawler.Metadata;
import com.digitalpebble.stormcrawler.indexing.AbstractIndexerBolt;
import com.digitalpebble.stormcrawler.persistence.Status;
import de.dlr.sc.crawler.ogms.URLEntity;
import de.dlr.sc.crawler.ogms.WebsiteEntity;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.neo4j.driver.exceptions.TransientException;
import org.neo4j.ogm.config.Configuration;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;
import org.neo4j.ogm.transaction.Transaction;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

/**
 * This Bolt reads status info from the status stream and creates
 * URL{@link URLEntity} and Website{@link WebsiteEntity} Nodes
 */
public class Neo4jBolt extends AbstractIndexerBolt {
    OutputCollector _collector;
    private static final org.slf4j.Logger LOG = LoggerFactory
            .getLogger(Neo4jBolt.class);
    Session session;
    SessionFactory sessionFactory;
    String neo4jServerAddress;

    public Neo4jBolt(String neo4jServerAddress) {
        this.neo4jServerAddress = neo4jServerAddress;
    }

    @Override
    public void prepare(Map conf, TopologyContext context,
                        OutputCollector collector) {
        super.prepare(conf, context, collector);
        _collector = collector;
        Configuration configuration = new Configuration.Builder()
                .uri(String.format("bolt://%s", neo4jServerAddress))
                .build();
        //Set up the Session
        sessionFactory = new SessionFactory(configuration, "de.dlr.sc.crawler.ogms");

    }

    @Override
    public void execute(Tuple tuple) {
        session = sessionFactory.openSession();
        String urlString = tuple.getStringByField("url");
        Metadata metadata = (Metadata) tuple.getValueByField("metadata");
        String[] refs = metadata.getValues("url.path");

        URL url;
        try {
            url = new URL(urlString);
            String domain = url.getHost();
            WebsiteEntity website = WebsiteEntity.getOrCreate(session, domain);
            String path = url.getFile();

            URLEntity urlEntry = URLEntity.getOrCreate(session, website, path, url.getProtocol());
            ArrayList<WebsiteEntity> websiteEntities = new ArrayList<>();
            ArrayList<URLEntity> urlEntities = new ArrayList<>();
            if (refs != null) {
                for (String ref : refs) {
                    if (ref.equals(urlString)) {
                        continue;
                    }
                    URL urlRef = new URL(ref);
                    String domainRef = urlRef.getHost();
                    WebsiteEntity websiteRef = WebsiteEntity.getOrCreate(session, domainRef);
                    String pathRef = urlRef.getFile();
                    URLEntity urlRefEntry = URLEntity.getOrCreate(session, websiteRef, pathRef, url.getProtocol());
                    websiteRef.addUrl(urlRefEntry);
                    urlEntry.outgoingUrlEntities.add(urlRefEntry);
                    websiteEntities.add(websiteRef);
                    urlEntities.add(urlRefEntry);
                }
            }
            try (Transaction tx = session.beginTransaction()) {
                if (!websiteEntities.isEmpty()) {
                    session.save(websiteEntities);
                }
                if (!urlEntities.isEmpty()) {
                    session.save(urlEntities);
                }
                session.save(urlEntry);
                website.addUrl(urlEntry);
                session.save(website);
                tx.commit();
            } catch (TransientException e) {
                LOG.error(String.valueOf(e));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        _collector.emit(
                com.digitalpebble.stormcrawler.Constants.StatusStreamName,
                tuple, new Values(urlString, metadata, Status.DISCOVERED));
        _collector.ack(tuple);
    }

    @Override
    public void cleanup() {
        if (sessionFactory != null)
            sessionFactory.close();
    }

}
