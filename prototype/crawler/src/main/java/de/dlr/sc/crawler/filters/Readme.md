# This package includes additional filter classes for the Storm Crawler.

**Domain Whitelist Filter:**

By including the class `de.dlr.sc.crawler.filters.DomainFilter` in the `urlfilters.json` file, 
domain filtering is enabled. 
This is a whitelist, which means that only pages are processed further if the TLD is included in the list.

Excerpt from urlfilters.json:

```javascript
{
	"com.digitalpebble.stormcrawler.filtering.URLFilters": [
		{
			"class": "de.dlr.sc.crawler.filters.DomainFilter",
			"name": "DomainFilter",
			"params": {
				"allowedDomains": ["de"] // TLDs without leading dot
			}
		},[...]
```