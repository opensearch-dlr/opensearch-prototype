package de.dlr.sc.crawler.ogms;

import org.checkerframework.checker.units.qual.A;
import org.neo4j.ogm.annotation.*;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.transaction.Transaction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NodeEntity
public class URLEntity {

    public URLEntity() {
    }

    public URLEntity(WebsiteEntity websiteEntity, String path, String protocol) {
        this.websiteEntity = websiteEntity;
        if (path.endsWith("/")) {
            this.path = path.substring(0, path.length() - 1);
        } else {
            this.path = path;
        }
        this.protocol = protocol;
        id = protocol + ":" + websiteEntity.domain + ":" + this.path;
    }

    public static URLEntity getOrCreate(Session session, WebsiteEntity websiteEntity, String path, String protocol) {
        URLEntity entity;
        try (Transaction tx = session.beginTransaction(Transaction.Type.READ_ONLY)) {
            entity = session.load(URLEntity.class, protocol + ":" + websiteEntity.domain + ":" + path);
        }
        if (entity == null) {
            return new URLEntity(websiteEntity, path, protocol);
        } else {
            return entity;
        }
    }

    @Property
    public String path;

    @Property
    public String protocol;

    @Id
    public String id;


    @Relationship(type = "BELONGS_TO", direction = Relationship.OUTGOING)
    public WebsiteEntity websiteEntity;

    @Relationship(type = "LINKS_TO", direction = Relationship.OUTGOING)
    public List<URLEntity> outgoingUrlEntities = new ArrayList<>();

    @Relationship(type = "LINKS_TO", direction = Relationship.INCOMING)
    public List<URLEntity> incomingUrlEntities = new ArrayList<>();

    @Override
    public String toString() {
        return "URLEntity{" +
                "path='" + path + '\'' +
                ", protocol='" + protocol + '\'' +
                ", id='" + id + '\'' +
                ", websiteEntity=" + websiteEntity +
                ", outgoingUrlEntities=" + outgoingUrlEntities +
                ", incomingUrlEntities=" + incomingUrlEntities +
                '}';
    }
}
