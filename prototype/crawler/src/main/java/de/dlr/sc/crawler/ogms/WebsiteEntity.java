package de.dlr.sc.crawler.ogms;

import org.neo4j.ogm.annotation.*;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.transaction.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@NodeEntity
public class WebsiteEntity {

    @Id
    @Property
    public String domain;

    @Relationship(type = "BELONGS_TO", direction = Relationship.INCOMING)
    public List<URLEntity> urlEntities = new ArrayList<>();

    public WebsiteEntity() {
    }

    public static WebsiteEntity getOrCreate(Session session, String domain) {
        WebsiteEntity entity;
        try (Transaction tx = session.beginTransaction(Transaction.Type.READ_ONLY)) {
            entity = session.load(WebsiteEntity.class, domain);
        }
        if (entity == null) {
            return new WebsiteEntity(domain);
        } else {
            return entity;
        }
    }

    public WebsiteEntity(String domain) {
        this.domain = domain;
    }

    public void addUrl(URLEntity urlEntity) {
        this.urlEntities.add(urlEntity);
    }

}
