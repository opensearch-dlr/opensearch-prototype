package de.dlr.sc.crawler.warc;

import com.digitalpebble.stormcrawler.util.ConfUtils;
import com.digitalpebble.stormcrawler.warc.WARCHdfsBolt;
import de.dlr.sc.crawler.Util.ReadConfigFromFile;
import de.dlr.sc.crawler.filters.DomainFilter;
import org.apache.storm.Config;
import org.apache.storm.hdfs.bolt.format.FileNameFormat;
import org.apache.storm.hdfs.bolt.rotation.FileSizeRotationPolicy;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Before the WarcBolt can be used in a topology, it must be configured in advance.
 * This can be done directly in the topology file, but it limits the readability of the topology configuration.
 * Therefore, this part is moved to this class.
 */
public class WarcHelper {

    @NotNull
    public static WARCHdfsBolt configure(Config conf) {

        String warcFileBasePath = ConfUtils.getString(conf, "warc.filePath");
        String crawlerName = ConfUtils.getString(conf, "crawlerName");
        String campaignFullPath = warcFileBasePath + "/" + crawlerName;

        if (Boolean.parseBoolean(ConfUtils.getString(conf, "warc.create.directory"))) {
            if (new File(warcFileBasePath).mkdir()) {
                System.out.println(warcFileBasePath + "created!");
                if (new File(campaignFullPath).mkdir()) {
                    System.out.println(campaignFullPath + "created!");
                } else {
                    System.out.println(campaignFullPath + "already existent!");
                }
            } else {
                System.out.println(ConfUtils.getString(conf, "warc.filePath") + "already existent!");
            }
        }
        try {
            FileWriter myWriter = new FileWriter(campaignFullPath+"/config.txt");
            String data = ReadConfigFromFile.getString(DomainFilter.class, "domain-regex.txt");
            assert data != null;
            myWriter.write(data);
            myWriter.write(String.valueOf(conf));
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        // WARC file config
        FileNameFormat fileNameFormat = new WARCFileNameFormat().withPath(campaignFullPath).withPrefix(crawlerName);
        Map<String, String> fields = new HashMap<>();
        fields.put("software:", ConfUtils.getString(conf, "warc.header.software"));
        fields.put("format", ConfUtils.getString(conf, "warc.header.format"));
        fields.put("conformsTo:", ConfUtils.getString(conf, "warc.header.conformsTo"));

        WARCHdfsBolt warcbolt = (WARCHdfsBolt) new WARCHdfsBolt()
                .withFileNameFormat(fileNameFormat);
        warcbolt.withHeader(fields);

        // enable WARC request records
        warcbolt.withRequestRecords();

        // a custom max length can be specified - 1 GB will be used as a default
        float count = Float.parseFloat(ConfUtils.getString(conf, "warc.fileSizeRotationPolicy.count"));
        String unit = ConfUtils.getString(conf, "warc.fileSizeRotationPolicy.unit");
        FileSizeRotationPolicy rotpol =
                new FileSizeRotationPolicy(count, FileSizeRotationPolicy.Units.valueOf(unit));
        warcbolt.withRotationPolicy(rotpol);

        warcbolt.withConfigKey("warc");
        Map<String, Object> hdfsConf = new HashMap<>();
        hdfsConf.put("fs.file.impl", "org.apache.hadoop.fs.RawLocalFileSystem");
        conf.put("warc", hdfsConf);
        return warcbolt;
    }
}
