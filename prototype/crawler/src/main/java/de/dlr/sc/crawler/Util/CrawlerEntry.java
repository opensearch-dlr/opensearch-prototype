package de.dlr.sc.crawler.Util;


import com.google.gson.Gson;

public class CrawlerEntry {
    public String name;
    public String domainRegex;
    public String subscribePattern;

    public CrawlerEntry(String name, String domainRegex, String subscribePattern) {
        this.name = name;
        this.domainRegex = domainRegex;
        this.subscribePattern = subscribePattern;
    }


    public static CrawlerEntry createFromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, CrawlerEntry.class);
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public CrawlerEntry() {
        // no-args constructor
    }
}
