package de.dlr.sc.crawler.Util;

public class PathEntry {
    public String path;
    public String protocol;
    public String foundBy;


    public PathEntry() {
        // No args constructor for B
    }

    public PathEntry setPath(String path) {
        this.path = path;
        return this;
    }

    public PathEntry setProtocol(String protocol) {
        this.protocol = protocol;
        return this;
    }

    public PathEntry setFoundBy(String crawlerName){
        this.foundBy = crawlerName;
        return this;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof PathEntry) {
            return ((PathEntry) other).protocol.equals(this.protocol) && ((PathEntry) other).path.equals(this.path);
        }
        return false;
    }
}
