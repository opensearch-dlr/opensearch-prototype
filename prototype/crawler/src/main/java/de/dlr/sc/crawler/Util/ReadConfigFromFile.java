package de.dlr.sc.crawler.Util;

import de.dlr.sc.crawler.filters.DomainFilter;
import org.apache.poi.ss.formula.functions.T;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

public class ReadConfigFromFile {

    public static String getString(Class clazz, String fileName) {
        InputStream is = clazz.getClassLoader().getResourceAsStream(fileName);
        if (is == null) {
            throw new IllegalArgumentException(fileName+" not found!");
        }
        try (InputStreamReader streamReader =
                     new InputStreamReader(is, StandardCharsets.UTF_8);
             BufferedReader reader = new BufferedReader(streamReader)) {
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
