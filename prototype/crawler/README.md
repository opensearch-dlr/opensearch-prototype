![Logo of the project](https://www.dlr.de/content/de/bilder/digitalisierung/opensearch.jpg?__blob=normal&v=3__ifc1920w)

# OpenSearch@DLR Prototype

The OpenSearch@DLR prototype will be used to investigate concepts for distributed crawling. This project is based on the
SDK called StormCrawler. This prototype will start indexing parts of the Internet based on a seed web address. If the
crawler encounters an unknown website, its content will be written to a Warc file. In addition, the way the crawler
reached the website is stored in the form of a website graph. If the crawler encounters a website for which it is not
responsible, it sends this website URL to a central key-value database. If other crawlers exist that are responsible for
this website, they are notified via this database and continue indexing from there. Further explanations about the
components, their function and how they cooperate can be found in the [wiki](https://gitlab.com/opensearch-dlr/opensearch-prototype/-/wikis/home).

## Getting started

### Initial Configuration

The first thing to do is to clone this repository with

```shell
git clone https://gitlab.com/opensearch-dlr/opensearch-prototype.git
```

and navigate into the prototype folder `prototype/crawler`.

In the `seeds.txt` file you have to specify the start address where the crawler starts its crawl
(for example: https://www.dlr.de/).

Further settings must be made in the file `crawler-conf.yaml`.

With these parameters you specify the addresses of the Neo4J and Redis server. In the docker setup, these parameters do
not need to be changed.

```shell
neo4jServerAddress: "neo4j"
redisServerAddress: "redis"
```

The `warc.filePath` specifies where the WARC files are stored. The path must be absolute and the folder must already
exist.

```shell
warc.filePath: "/crawler/warc" # For Linux systems, for windows it may looks like "C:\User\someuser\Documents\warcfiles"
```

Further elasticsearch related configuration is located at `es-conf.yaml`. If docker is used, no further configuration is
needed.

The file `src/main/resources/urlfilters.json` specifies which TLDs are to be indexed.

```json lines
{
  "class": "de.dlr.sc.crawler.filters.DomainFilter",
  "name": "DomainFilter",
  "params": {
    "allowedDomains": [
      "de" // accepted TLDs
    ]
  }
}
```

### Start instance using docker

To get started easily, a docker-compose.yml and a Makefile are provided. This requires Docker and GNU make to be
installed.

```shell
make build-topology: # Build storm crawler artefact
make init # Initializes Elasticsearch and Kibana with the proper indexes
make start-local # Runs the whole stack
```

The first step is to generate the Uberjar via maven using `build-topology`. This JAR contains the prototype.
`make init` initializes elasticsearch with the required index and kibana with two simple dashboards that can be used to
track the progress of the indexing. Finally, a local, not distributed, instance is started with `start-local`. When the
instance is completely started, the status of the crawl can be monitored via different web addresses.

* Elasticsearch: http://localhost:9200
* Kibana: http://localhost:5601
* Storm ui: http://localhost:8080
* Redis: http://localhost:8081
* Neo4j: http://localhost:7687

## Developing

### Building

The source code of the prototype can be found at `prototype/crawler/src/main/java/en/dlr/sc/crawler`. If changes have
been made to the source code, the topology must be regenerated with `make build-topology`.

```shell
make clean # shutdown running instance and delete containers
make init # Optional: Delete elasticsearch index already filled with previous crawling results
make build-topology # 
make start-local # Rebuild storm crawler artefact with new changes
```

## Links

Even though this information can be found inside the project on machine-readable format like in a .json file, it's good
to include a summary of most useful links to humans using your project. You can include links like:

- Project homepage: https://www.dlr.de/content/en/articles/digitalisation/digitalisation-project-opensearchatdlr.html
- Repository: https://gitlab.com/opensearch-dlr/opensearch-prototype
- Issue tracker: https://gitlab.com/opensearch-dlr/opensearch-prototype/-/issues
    - In case of sensitive bugs like security vulnerabilities, please contact opensearch@dlr.de directly instead of
      using issue tracker. We value your effort to improve the security and privacy of this project!
- Related projects:
    - Storm Crawler repository: https://github.com/DigitalPebble/storm-crawler

## Licensing

The code in this project is licensed under Apache License 2.0 licence.
