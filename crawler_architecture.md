# Opensearch prototype -  DLR Parallel Crawler - Architecture

### Overview 

In this document, we address the architecture for a parallel web crawler for DLR. 

The main responsibility of the crawler is downloading and storing Web pages - here we focus only on three domains: elib.de, DLR's GitLab and DLR Wiki.

As an overview, a crawler starts with a set of URLs, U_0, in a queue where all the URLs are managed. For each URL, the crawler downloads the page and add the URLs extracted from this page to the queue. This process is repeated until the crawler stops. And the collected pages are used later for information retrieval applications.

To increase the speed of this task, we would use a parallel crawler, where multiple processes run in parallel to download pages. However, our design must take into consideration the balance between maximizing its download rate and minimizing its overhead.

We based our design on the paper **Parallel Crawler**[1]. As mentioned by [1], several challenges arise when using a parallel crawler:

- Pages Overlap: when several processes run in parallel, the same page can be downloaded several times.
- Quality: define the importance of the page to download.
- Communication bandwidth between the crawler processes: the processes need to communicate to ensure low page overlap and high page quality. However, this bandwidth can be minimized while ensuring effective crawling activities. 


###  Architecture

We use a *distributed crawler* assuming that different crawler processes will be communicating via the Internet and not only intranet. For our crawler processes to communicate, we will use *static assignment* (unlike independent assignment which leads to a huge overlap in pages, and dynamic assignment which leads to a bottleneck from the coordinator responsible for assigning dynamic URLs). With static assignment, the web is partitioned and each partition is assigned to a crawler process. 

###  Crawling Modes

As defined by [1], under static assignment, each crawling process is responsible for a certain partition of the web. However, some pages in the partition may refer to links in certain partitions (inter-partition links (IPL)). To deal with that, they define the following modes:

- Firewall mode: downloads only links in its partition and ignores the inter-partition links. This will leads to missing some pages but with zero-overlap and no runtime coordination.
- Cross-over mode: They follow the links discover by themselves, including IPL. This will leads to downloading more pages than the firewall mode but with some overlap and no runtime coordination.
- *Exchange mode*: when crawling processes interchange IPL. Each process downloads the pages that it is responsible for and it informs the other process of any IPL. Exchange mode has more coverage than the firewall mode, less overlap than the cross-over mode, but the runtime coordination is higher.

We use the *Exchange mode* to ensure a higher coverage and low pages overlap.

#### URL Exchange

The exchange mode requires communication between crawling processing to exchange the inter-partition links (IPL).

- Batch communication: send a set of IPLs (a batch) after downloading k pages, to the appropriate processor. However, the processor does not memorize all the previously discovered IPLs. It rather memorizes only the current batch IPLs; this can lead to higher page overlap and less runtime coordination.
- **Replication**: it is known that a small number of pages has a large number of incoming links (Zipfian distribution). Therefore, [1] suggests identifying the top k popular pages from a previous image and send them to all processes, and stop transferring them between processes.


#### Partitioning Function
We adapt the site-hash-based function: the hash value is computed on the level of the site map. for example, elib.de will be assigned to 1 proc.

### Evaluation
- Overlap: minimize (N-I)/I where N: number of all pages and I: number of unique pages. Since we will use the *exchange mode*, the overlap is zero.
- Coverage: number of downloaded pages/number of all pages.
- Quality: we need to define an *importance metric* for the crawler
    - for *elib*: it could be based on metadata such as citations and number of downloads/views.
    - for *GitLab*: number of stars and watchers.
    - for *wiki*: number of views and likes. 
- Communication overhead: the average number of the IPLs exchanged per page.
    
## Suggested Architecture

### Summary of the Options Selected for our Parallel Crawler 

![Parallel Crawler Architecture](diagrams/overview_parallel_crawler_architecture_selections.png)


### Summary of our Parallel Crawler 

![Parallel Crawler Architecture](diagrams/parallel_crawler_architecture.png)

### Coordination between Nodes
Crawling processes will be handled by crawler nodes completely autonomous. However, we forsee a single trusted instance that hold information about responsibilities (i.e. sub-domains of crawler nodes). Every node can access this information in order to determine where to send a URL out of the own responsibility.

This information is stored in a table similar to the one below:

|URL_pattern|crawler_location|minimum_batch_size|
--- | --- | ---
|*wiki.dlr.de|https://wiki_crawler.dlr.de|10|
|*elib.dlr.de|https://elib_crawler.dlr.de|50|
|*gitlab.dlr.de|https://gitlab_crawler.de|2|
|...|...|...|

The url pattern is used by crawlers to determine responsible nodes for a specific URL and send it via a socket connection located at 'crawler_location'. In order to reduce bandwidth and communication overhead, every crawler node defines a minimum batch size. One crawler instance caches urls from another's repsonsibilty up to this value and send them at once in a single batch.


## Technologies

- Crawling: [Heritrix](https://github.com/internetarchive/heritrix3), StormCrawler, Apache Nutch
- URL prioritization: “heapq” Python library
- Crawling dynamic content: PyQT5, Splash, Selenium

### References 

[1] Cho, Junghoo, and Hector Garcia-Molina. "Parallel crawlers." Proceedings of the 11th international conference on World Wide Web. 2002.

